import 'dart:io';

import 'package:easy_dialog/easy_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:path_provider/path_provider.dart';


class CompressedImageScreenPage extends StatefulWidget {
  String? imageSize;
  File? originalImage;
  File? compressedImage;
  String compressedImagePath = "/storage/emulated/0/Download/";

  CompressedImageScreenPage(this.imageSize, this.originalImage,
      this.compressedImage, this.compressedImagePath);

  @override
  State<CompressedImageScreenPage> createState() =>
      _CompressedImageScreenPageState();
}

class _CompressedImageScreenPageState extends State<CompressedImageScreenPage> {
  int size = 48;
  int desiredSize = 48;

  // double _progressValue = 0.0;
  bool _isCompressed = false;


  var sizeController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sizeController.text = size.toString();
  }

  Future compressImage() async {
    final tempDir = await getTemporaryDirectory();
    final path = '${tempDir.path}/${DateTime
        .now()
        .millisecond}.jpg';

    if (widget.originalImage == null) {
      return;
    }

    final fileSize = await File(widget.originalImage!.path).length();

    if (fileSize <= (desiredSize * 1024)) {
      return;
    }

    EasyDialog(
      title: Text(
        'Compressing image...',
        style: TextStyle(fontSize: 20),
      ),
      contentList: [
        CircularProgressIndicator()
        // LinearProgressIndicator(
        //   value: _progressValue,
        //   minHeight: 10.0,
        //   backgroundColor: Colors.grey[300],
        //   valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
        // ),
      ],
      closeButton: false,
    ).show(context);

    var compressedFile = await FlutterImageCompress.compressAndGetFile(
        widget.originalImage!.path, path.replaceAll('.jpg', '_compressed.jpg'),
        quality: 90);


    var compressedFileSize = await compressedFile!.length();
    if (compressedFileSize <= (desiredSize * 1024)) {
      setState(() {
        widget.compressedImage = compressedFile;
      });
      Navigator.pop(context);
      return;
    }

    int quality = 85;
    // double progressValue = 0.0;
    while (compressedFileSize > (desiredSize * 1024) && quality >= 50) {
      print("quality:$quality");
      compressedFile!.deleteSync();
      compressedFile = await FlutterImageCompress.compressAndGetFile(
        widget.originalImage!.path,
        path.replaceAll('.jpg', '_compressed.jpg'),
        quality: quality,
      );
      compressedFileSize = await compressedFile!.length();
      quality -= 1;
      // progressValue = 1.0 - (quality - 50) / 70;
      // setState(() {
      //   _progressValue = progressValue;
      // });
    }

    // while (await compressedFile!.length() > (desiredSize * 1024)) {
    //   quality -= 2;
    //   compressedFile = await FlutterImageCompress.compressAndGetFile(
    //     compressedFile.path,
    //     compressedFile.path.replaceAll('.jpg', '_compressed.jpg'),
    //   );
    // }

    // var compressedFileSize = await compressedFile!.length();
    // print(compressedFileSize);

    // while (compressedFileSize > (desiredSize*1024)+5000 || compressedFileSize < (desiredSize*1024) - 5000) {
    //   if(compressedFileSize > (desiredSize*1024)+5000){
    //     quality-=2;
    //   }
    //   print("quality:$quality");
    //   try{
    //       compressedFile = await FlutterImageCompress.compressAndGetFile(
    //         compressedFile!.path,
    //         compressedFile.path.replaceAll(".jpg", "_compressed.jpg"),
    //         quality: quality,
    //         minWidth: 500,
    //         minHeight: 500
    //         // minHeight: MediaQuery.of(context).size.width.toInt()~/2,
    //         // minWidth: MediaQuery.of(context).size.height.toInt()~/2
    //       );
    //     compressedFileSize = await compressedFile!.length();
    //   } catch(e){
    //     print("Error: $e");
    //     break;
    //   }
    // }

    // while(compressedFileSize > (desiredSize*1024)){
    //
    // }

    setState(() {
      widget.compressedImage = compressedFile;
      _isCompressed = true;
    });
    Navigator.of(context).pop();
    print("original image:${await widget.originalImage!.length()}");
    print("compressed image:${await compressedFile!.length()}");
    if (await compressedFile.length() < await widget.originalImage!.length()) {
      print("successfully compressed");
    }
  }

  Future<void> _saveImageToGallery() async {
    try {
      final result = await GallerySaver.saveImage(widget.compressedImage!.path);
      print('Image saved to gallery. Result = $result');
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Image saved successfully'),
        ),
      );
    } catch (e) {
      print("Error:$e");
      print("not saved");
    }
  }

  @override
  Widget build(BuildContext context) {
    print('desired Size is:${desiredSize}');
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black45,
          title: Text("Image Compression"),
          actions: [
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 0,
                child: Container(
                  width: double.infinity,
                  child: Card(
                    elevation: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Photo Information",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text("Image Size: "),
                            Text(
                              "${widget.imageSize.toString()}",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 0,
                child: Container(
                  margin: EdgeInsets.only(top: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Expected Size",
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Card(
                              child: TextFormField(
                                textAlign: TextAlign.center,
                                textAlignVertical: TextAlignVertical.bottom,
                                controller: sizeController,
                                keyboardType: TextInputType.number,
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.w500),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Center(
                              child: Container(
                                margin: EdgeInsets.only(top: 5),
                                child: Text(
                                  "KB",
                                  style: TextStyle(fontSize: 20),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Slider(
                              value: size.toDouble(),
                              onChanged: (double value) {
                                setState(() {
                                  size = value.toInt();
                                  desiredSize = value.toInt();
                                  sizeController.text = size.toString();
                                });
                              },
                              min: 48,
                              max: 1048,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    _isCompressed ? Container(
                      color: Colors.black,
                      child: TextButton(
                        onPressed: () {
                          _saveImageToGallery();
                        },
                        child: Text("Save To Gallery", style: TextStyle(
                            fontSize: 20, color: Colors.white),),),
                    ) : Text(""),
                    Card(
                      elevation: 2,
                      color: Colors.blue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30)),
                      child: Container(
                        width: double.infinity,
                        child: TextButton(
                          onPressed: () {
                            compressImage();
                          },
                          child: Text(
                            "Compress",
                            style:
                            TextStyle(fontSize: 20, color: Colors.white),
                          ),),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
