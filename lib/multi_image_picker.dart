import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerWidget extends StatefulWidget {
  @override
  _ImagePickerWidgetState createState() => _ImagePickerWidgetState();
}

class _ImagePickerWidgetState extends State<ImagePickerWidget> {
  List<File> _imageList = [];

  Future<void> _pickImages() async {
    List<XFile>? pickedFiles = await ImagePicker().pickMultiImage();
    if (pickedFiles != null) {
      List<File> pickedImages =
          pickedFiles.map((XFile file) => File(file.path)).toList();
      setState(() {
        _imageList.addAll(pickedImages);
        for (File imageFile in pickedImages) {
          int fileSizeInBytes = imageFile.lengthSync();
          double fileSizeInKB = fileSizeInBytes / 1024;
          print("Image size: ${fileSizeInKB.toStringAsFixed(2)} KB");
        }
      });
    }
  }

  Future<File> compressImage(File file) async {
    // Compress the image file
    File? compressedImageFile = await FlutterImageCompress.compressAndGetFile(
      file.path,
      file.path + '_compressed.jpg',
      quality: 70, // Set the image quality (0-100)
    );

    // Print the compressed image size to the console
    int compressedFileSizeInBytes = compressedImageFile!.lengthSync();
    double compressedFileSizeInKB = compressedFileSizeInBytes / 1024;
    print("Compressed image size: ${compressedFileSizeInKB.toStringAsFixed(2)} KB");

    // Return the compressed image file
    return compressedImageFile;
  }

  Future<List<File>> compressImages(List<File> imageList) async {
    List<File> compressedImageList = [];
    for (File imageFile in imageList) {
      File compressedImageFile = await compressImage(imageFile);
      compressedImageList.add(compressedImageFile);
    }
    return compressedImageList;
  }

  bool canAddMoreImages(){
    return _imageList.length < 9;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Image Picker Widget'),
      ),
      floatingActionButton: FloatingActionButton.large(
          onPressed: () {
            compressImages(_imageList);
          }, child: Text("compress")),
      body: Row(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.black,
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              children: [
                Expanded(
                  flex: 2,
                  child: GridView.builder(
                    itemCount: _imageList.length + 1,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        mainAxisSpacing: 4.0,
                        crossAxisSpacing: 4.0),
                    itemBuilder: (context, index) {
                      return Container(
                        color: _imageList.length<9?Colors.red:null,
                        child: index == _imageList.length
                            ? InkWell(
                                onTap: () => canAddMoreImages() ?_pickImages():null,
                                child: _imageList.length<9?Icon(Icons.add):null)
                            : Image.file(
                                _imageList[index],
                                fit: BoxFit.cover,
                              ),
                      );
                    },
                  ),
                ),
                Expanded(
                  child: Container(
                    color: Colors.pink,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.black,
            ),
          )
        ],
      ),
    );
  }
}
