import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:image_compression_app/compress_image_screen.dart';
import 'package:image_picker/image_picker.dart';

class ImageScreenPage extends StatefulWidget {
  @override
  State<ImageScreenPage> createState() => _ImageScreenPageState();
}

class _ImageScreenPageState extends State<ImageScreenPage> {
  File? originalImage;
  File? compressedImage;
  String compressedImagePath = "/storage/emulated/0/Download/";
  bool multipleImagesPicked =  false;

  List<File> _imageList = [];

  Future<void> _pickImages() async {
    List<XFile>? pickedFiles = await ImagePicker().pickMultiImage();
    if (pickedFiles != null) {
      List<File> pickedImages =
          pickedFiles.map((XFile file) => File(file.path)).toList();
      setState(() {
        _imageList.addAll(pickedImages);
      });
    }
  }

  Future getImage(ImageSource source) async {

    final image = await ImagePicker.platform.pickImage(source: source);
    if (image != null) {
      setState(() {
        originalImage = File(image.path);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // print(_image!.lengthSync()*0.00097656);
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
              title: Text("Image Compression"),
              backgroundColor: Colors.black45),
          body: Column(
            children: [
              if (originalImage==null) Expanded(
                flex: 0,
                child: Center(
                  child: Container(
                    // color: Colors.black,
                    margin: EdgeInsets.only(top: 10),
                    height: 300,
                    width: 300,
                    // color: Colors.black,
                    child: GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        mainAxisSpacing: 4.0,
                        crossAxisSpacing: 4.0,
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        if (index == _imageList.length) {
                          return InkWell(
                            onTap: () {
                              multipleImagesPicked = true;
                              _pickImages();
                            },
                            child: Icon(Icons.add),
                          );
                        } else {
                          return GridTile(
                            child:
                                Image.file(_imageList[index], fit: BoxFit.cover),
                          );
                        }
                      },
                      itemCount: _imageList.length + 1,
                    ),
                  ),
                ),
              ) else Text(""),
              originalImage != null && multipleImagesPicked == false
                  ? Expanded(
                      flex: 2,
                      child: Container(
                        margin: EdgeInsets.all(10),
                        child: Column(
                          // mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Center(
                              child: originalImage == null
                                  ? Text("image")
                                  : Image.file(
                                      originalImage!,
                                      height: 300,
                                      width: 300,
                                    ),
                            ),
                            originalImage == null
                                ? Text("")
                                : Text(
                                    "${(originalImage!.lengthSync() * 0.00097656).toStringAsFixed(2)} kB",
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  )
                          ],
                        ),
                      ),
                    )
                  : Text(""),
              multipleImagesPicked == false?Expanded(
                child: Container(
                  // color: Colors.blue,
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: originalImage == null
                        ? MainAxisAlignment.center
                        : MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        margin: originalImage == null
                            ? EdgeInsets.only(bottom: 5)
                            : EdgeInsets.only(bottom: 0),
                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: TextButton(
                          onPressed: () {
                            getImage(ImageSource.camera);
                          },
                          child: Text(
                            "Choose From Camera",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.black,
                            borderRadius: BorderRadius.circular(10)),
                        child: TextButton(
                          onPressed: () {
                            getImage(ImageSource.gallery);
                          },
                          child: Text(
                            "Choose From Gallery",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      originalImage != null
                          ? Container(
                              decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.circular(10)),
                              child: TextButton(
                                onPressed: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) {
                                        return CompressedImageScreenPage(
                                            "${(originalImage!.lengthSync() * 0.00097656).toStringAsFixed(2)} kB",
                                            originalImage,
                                            compressedImage,
                                            compressedImagePath);
                                      },
                                    ),
                                  );
                                },
                                child: Text(
                                  "Compress Photo",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            )
                          : Text(""),
                    ],
                  ),
                ),
              ):Text(""),
              multipleImagesPicked==true?Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      color: Colors.blue,
                      child: TextButton(onPressed: () {
                        print("on to next screen");
                      },
                          child: Text("Compress Images",style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,),),),
                    ),
                  ],
                ),
              ):Text("")
            ],
          )),
    );
  }
}
